package com.worldofautomation.webautomationpnc;

import com.worldofautomation.webautomation.Utilities;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.annotations.DataProvider;

public class DataGenerator {

    @DataProvider(name ="getZipCode")
    public Object[] getZipCode(){
        JSONArray jsonArray = Utilities.getJSONArray("src/test/resources/TestData.json");
        JSONObject jsonObject = (JSONObject) jsonArray.get(0);
        String data = (String) jsonObject.get("ZipCode");
        Object[] objects = new Object[1];
        objects[0] = data;
        return  objects;
    }
    @DataProvider(name ="getZipCode2")
    public Object[] getZipCode2(){
        JSONArray jsonArray = Utilities.getJSONArray("src/test/resources/TestData.json");
        JSONObject jsonObject = (JSONObject) jsonArray.get(0);
        String data = (String) jsonObject.get("ZipCode");
        Object[] objects = new Object[1];
        objects[0] = data;
        return  objects;
    }

}
