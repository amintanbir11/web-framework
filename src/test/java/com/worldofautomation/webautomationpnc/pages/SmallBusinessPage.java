package com.worldofautomation.webautomationpnc.pages;

import com.worldofautomation.webautomation.ExtentTestManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class SmallBusinessPage {
    @FindBy(xpath = "(//p[@class='heading'])[1]")
    private WebElement resultOfValidation;

    @FindBy(xpath = "(//p[@class='heading'])[1]")
    private WebElement BusinessCheckingBtn;

    @FindBy(xpath = "(//div[@class='white-bg']/h1)[1]")
    private WebElement pageValidationOfBusinessChecking;

    @FindBy(xpath = "//a[@href='https://forms.pnc.com/content/pnc-bbdda-forms/en/business-checking.html#']")
    private WebElement ApplyOnlineBtnForBC;


    public void validateUserIsOnSmallBusinessPage(){
       String results =  resultOfValidation.getText();
        Assert.assertTrue(results.contains("Business Checking"));
        ExtentTestManager.log("User is on The Small Business Page");
    }
    public void clickOnTheBusinessCheckingButton(){
        BusinessCheckingBtn.click();
        ExtentTestManager.log("clicked on Business Checking");
    }
    public void validateUserIsOnBusinessCheckingPage(){
       String text = pageValidationOfBusinessChecking.getText();
       Assert.assertTrue(text.contains("Small Business Checking Accounts"));
       ExtentTestManager.log("user is on the Business Checking Page");
    }
    public void clickOnApplyOnlineForBusinessCheckingAccount(){
        ApplyOnlineBtnForBC.click();
        ExtentTestManager.log("clicked on the Apply Online");
    }
}
