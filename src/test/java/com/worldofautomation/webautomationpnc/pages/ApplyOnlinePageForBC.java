package com.worldofautomation.webautomationpnc.pages;

import com.worldofautomation.webautomation.ExtentTestManager;
import com.worldofautomation.webautomation.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.testng.Assert;

public class ApplyOnlinePageForBC {

    @FindBy(xpath = "//h2[text()='IMPORTANT INFORMATION ABOUT PROCEDURES FOR OPENING A NEW ACCOUNT']")
    private WebElement textForPageValidation;

    @FindBy(xpath = "//input[@aria-label='Business Zip Code']")
    private WebElement zipCodeField;

    public void validateUserIsOnApplyOnlineBCPage(){
        TestBase.waitFor(3);
        boolean text = textForPageValidation.isDisplayed();
        Assert.assertTrue(text,"is nOtDisplayed");
        ExtentTestManager.log("Apply Online for Business Checking Page is Validated");
    }
    public void sendZipCodeToStartTheApplication(String data){
        TestBase.sendKeysByXpath("//input[@aria-label='Business Zip Code']", data);
        ExtentTestManager.log("Zip Code is field with data");
    }
}
