package com.worldofautomation.webautomationpnc.pages;

import com.worldofautomation.webautomation.ExtentTestManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage {

    @FindBy(xpath = "//a[@class='level-1 theme_green']")
    private WebElement SmallBusiness;

    public void clickOnSmallBusiness(){
        SmallBusiness.click();
        ExtentTestManager.log("clicked on the Small Business");
    }
}
