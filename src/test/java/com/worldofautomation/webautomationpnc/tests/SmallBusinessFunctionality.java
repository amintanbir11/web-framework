package com.worldofautomation.webautomationpnc.tests;

import com.worldofautomation.webautomation.TestBase;
import com.worldofautomation.webautomationpnc.DataGenerator;
import com.worldofautomation.webautomationpnc.pages.ApplyOnlinePageForBC;
import com.worldofautomation.webautomationpnc.pages.HomePage;
import com.worldofautomation.webautomationpnc.pages.SmallBusinessPage;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class SmallBusinessFunctionality extends TestBase {
    private HomePage homePage;
    private SmallBusinessPage smallBusinessPage;
    private ApplyOnlinePageForBC applyOnlinePageForBC;

    @BeforeMethod
    private void setUpInstances() {
        homePage = PageFactory.initElements(getDriver(), HomePage.class);
        smallBusinessPage = PageFactory.initElements(getDriver(), SmallBusinessPage.class);
        applyOnlinePageForBC = PageFactory.initElements(getDriver(), ApplyOnlinePageForBC.class);
    }


    @Test(dataProviderClass = DataGenerator.class,dataProvider = "getZipCode")
    public void userIsCapableOfOpeningBusinessCheckingAccountOnline(String data) {
        homePage.clickOnSmallBusiness();
        smallBusinessPage.validateUserIsOnSmallBusinessPage();
        smallBusinessPage.clickOnTheBusinessCheckingButton();
        smallBusinessPage.validateUserIsOnBusinessCheckingPage();
        smallBusinessPage.clickOnApplyOnlineForBusinessCheckingAccount();
        applyOnlinePageForBC.validateUserIsOnApplyOnlineBCPage();
        applyOnlinePageForBC.sendZipCodeToStartTheApplication(data);
    }


}
