package com.worldofautomation.webautomationebay.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegisterPage {
    @FindBy(id = "firstname")
    private WebElement firstname;
    @FindBy(id = "lastname")
    private WebElement lastname;


    public void fillFirstAndLastName() {
        firstname.sendKeys("world");
        lastname.sendKeys("NoPassword");
    }
}
