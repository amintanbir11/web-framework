package com.worldofautomation.webautomationebay.pages;

import com.worldofautomation.webautomation.ExtentTestManager;
import com.worldofautomation.webautomation.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class HomePage {

    @FindBy(linkText = "register")
    private WebElement registerBtn;

    @FindBy(id = "gh-ac")
    private WebElement searchBar;

    @FindBy(id = "gh-btn")
    private WebElement searchBtn;

    public void clickOnRegisterButton(){
        registerBtn.click();
    }
    public void validateUserOnHomePage(){
        String currentUrl = TestBase.getDriver().getCurrentUrl();
        String expectedUrl = "https://www.ebay.com/";
        Assert.assertEquals(currentUrl,expectedUrl);
        ExtentTestManager.log("url has been validated");
    }
    public void typeOnSearchBar(String data){
        searchBar.sendKeys(data);
        ExtentTestManager.log("Java Books has been typed");
    }
    public void clickOnSearchButton(){
        searchBtn.click();
        ExtentTestManager.log("Search Button Has Been clicked ");
    }
}
