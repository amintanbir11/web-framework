package com.worldofautomation.webautomationebay.pages;

import com.worldofautomation.webautomation.ExtentTestManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class
SearchResultPage {

    @FindBy(xpath = "//h1[@class='srp-controls__count-heading']")
    private WebElement resultForValidation;

    public void validateUserOnSearchResultPage(String data){
        String result = resultForValidation.getText();
        System.out.println(result);
        Assert.assertTrue(result.contains("results for " + data));
        ExtentTestManager.log("results for "+data+" is displayed");
    }
}
