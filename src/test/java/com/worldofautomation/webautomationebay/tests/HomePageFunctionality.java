package com.worldofautomation.webautomationebay.tests;

import com.worldofautomation.webautomation.ConnectSQL;
import com.worldofautomation.webautomation.ExtentTestManager;
import com.worldofautomation.webautomation.TestBase;
import com.worldofautomation.webautomationebay.DataGenerator;
import com.worldofautomation.webautomationebay.Queries;
import com.worldofautomation.webautomationebay.pages.HomePage;
import com.worldofautomation.webautomationebay.pages.SearchResultPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.worldofautomation.webautomationebay.pages.RegisterPage;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class HomePageFunctionality extends TestBase {
    private HomePage homePage;
    private RegisterPage registerPage;
    private SearchResultPage searchResultPage;

    @BeforeMethod
    private void setUpInstances() {
        homePage = PageFactory.initElements(getDriver(), HomePage.class);
        registerPage = PageFactory.initElements(getDriver(), RegisterPage.class);
        searchResultPage = PageFactory.initElements(getDriver(), SearchResultPage.class);
    }

    @Test(enabled = false)
    public void quickTest() {

        homePage.clickOnRegisterButton();
        ExtentTestManager.log("clicked");
        registerPage.fillFirstAndLastName();
        ExtentTestManager.log("filled");
    }

    @Test(dataProviderClass = DataGenerator.class,dataProvider = "getSearchData", enabled = false)
    public void userIsBeingAbleToTypeOnSearchBarOfTheirPreference(String data) {
        homePage.validateUserOnHomePage();
        homePage.typeOnSearchBar(data);
        homePage.clickOnSearchButton();
        searchResultPage.validateUserOnSearchResultPage(data);
    }

    @Test
    public void userIsBeingAbleToTypeOnSearchBarOfTheirPreferenceDB() throws SQLException {
        String data = "";
        Connection connection = ConnectSQL.getConnection("batch3");
        Statement statement = ConnectSQL.getStatement(connection);
        ResultSet resultSet = ConnectSQL.getResultSet(statement, Queries.SELECT_Query);
        ArrayList<String> bookList = new ArrayList<>();
        while (resultSet.next()){
            bookList.add(resultSet.getString("Books"));
        }

        homePage.validateUserOnHomePage();
        homePage.typeOnSearchBar(bookList.get(1));
        homePage.clickOnSearchButton();
        searchResultPage.validateUserOnSearchResultPage(bookList.get(1));
    }
}
