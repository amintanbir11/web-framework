package com.worldofautomation.webautomationebay;

import com.worldofautomation.webautomation.Utilities;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.annotations.DataProvider;

public class DataGenerator {

    @DataProvider(name = "getSearchData")
    public Object[] getSearchData() {
        JSONArray jsonArray = Utilities.getJSONArray("src/test/resources/TestData.json");
        JSONObject jsonObject = (JSONObject) jsonArray.get(2);
        String data = (String) jsonObject.get("SearchData");
        Object[] objects = new Object[1];
        objects[0] = data;
        return objects;

    }

}
