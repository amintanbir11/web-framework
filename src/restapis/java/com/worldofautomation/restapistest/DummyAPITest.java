package com.worldofautomation.restapistest;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;

public class DummyAPITest {

    @Test
    public void getAllTheEmployees(){
        RestAssured.baseURI= "http://dummy.restapiexample.com/api/v1";

        Response response = RestAssured.given().when().get("/employees")
                .then().assertThat().statusCode(200).extract().response();
        System.out.println(response.body().prettyPrint());
    }
    @Test
    public void getSingleEmployees(){
        RestAssured.baseURI= "http://dummy.restapiexample.com/api/v1";

        Response response = RestAssured.given().when().log().all().get("/employee/1")
                .then().assertThat().statusCode(200).extract().response();
        System.out.println(response.body().prettyPrint());
    }
    @Test
    public void deleteAEmployees(){
        RestAssured.baseURI= "http://dummy.restapiexample.com/api/v1";

        Response response = RestAssured.given().when().delete("/delete/1")
                .then().assertThat().statusCode(200).extract().response();
        System.out.println(response.body().prettyPrint());
    }
    @Test
    public void createUser(){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name","Tanbir");
        jsonObject.put("salary","120000");
        jsonObject.put("age","26");

        RestAssured.baseURI= "http://dummy.restapiexample.com/api/v1";

        Response response = RestAssured.given().when().body(jsonObject.toString()).log().all()
        .post("/create").then().assertThat().statusCode(200).extract().response();
        System.out.println(response.body().prettyPrint());
    }
    @Test
    public void updateUser(){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name","Tanbir");
        jsonObject.put("salary","125000");
        jsonObject.put("age","26");

        RestAssured.baseURI= "http://dummy.restapiexample.com/api/v1";

        Response response = RestAssured.given().when().body(jsonObject.toString()).log().all()
                .put("/update/27").then().assertThat().statusCode(200).extract().response();
        System.out.println(response.body().prettyPrint());
    }
}
