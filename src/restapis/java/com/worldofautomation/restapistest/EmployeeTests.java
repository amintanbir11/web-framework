package com.worldofautomation.restapistest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mysql.cj.jdbc.ConnectionImpl;
import com.worldofautomation.models.Employees;
import com.worldofautomation.webautomation.ConnectSQL;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class EmployeeTests {

    //http://localhost:8090/employees

    @Test
    public void getAllTheEmployees() throws JsonProcessingException, SQLException {
        RestAssured.baseURI = "http://localhost:8090/employees";
        Response response = RestAssured.given().when().log().all().get("/all")
                .then().assertThat().statusCode(200).extract().response();
        System.out.println(response.body().prettyPrint());
        //Store all the data into a list of class.
        ObjectMapper objectMapper = new ObjectMapper();

        //List<Employees> employees = objectMapper.readValue(response.asString(),List.class);

        //code below is to see the individual record of employee.
        List<Employees> employeesFromAPI = objectMapper.readValue(response.asString(),
                objectMapper.getTypeFactory().constructCollectionType(List.class, Employees.class));

        Connection connection = ConnectSQL.getConnection("worldOfAutomation");
        Statement statement = ConnectSQL.getStatement(connection);
        ResultSet resultSet = ConnectSQL.getResultSet(statement, "SELECT * FROM worldOfAutomation.employee");
        List<Employees> employeesFromDB = new ArrayList<>();

        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            boolean permanent = resultSet.getBoolean("permanent");
            int phone_number = resultSet.getInt("phone_number");
            String role = resultSet.getString("role");
            String city = resultSet.getString("city");
            Employees employees = new Employees(id, name, permanent, phone_number, role, city);
            employeesFromDB.add(employees);
        }
        Assert.assertEquals(employeesFromAPI.toString(), employeesFromDB.toString());
    }

    @Test
    public void getSingleEmployee() throws JsonProcessingException, SQLException {
        RestAssured.baseURI = "http://localhost:8090/employees";
        Response response = RestAssured.given().when().log().all()
                .get("/1").then().assertThat().statusCode(200).extract().response();
        ObjectMapper objectMapper = new ObjectMapper();
        Employees employeesFromAPI = objectMapper.readValue(response.asString(), Employees.class);

        Connection connection = ConnectSQL.getConnection("worldOfAutomation");
        Statement statement = ConnectSQL.getStatement(connection);
        ResultSet resultSet = ConnectSQL.getResultSet(statement, "SELECT * FROM worldOfAutomation.employee where id =1;");
        Employees employeesFromDB = null;
        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            boolean permanent = resultSet.getBoolean("permanent");
            int phone_number = resultSet.getInt("phone_number");
            String role = resultSet.getString("role");
            String city = resultSet.getString("city");
            employeesFromDB = new Employees(id, name, permanent, phone_number, role, city);
        }
        Assert.assertEquals(employeesFromAPI.toString(),employeesFromDB.toString());
    }
    @Test
    public void deleteData() throws SQLException {
        RestAssured.baseURI = "http://localhost:8090/employees";
        Response response = RestAssured.given().when().log().all().delete("/delete/2").then()
                .assertThat().statusCode(204).extract().response();
        Connection connection = ConnectSQL.getConnection("worldOfAutomation");
        Statement statement = ConnectSQL.getStatement(connection);
        ResultSet resultSet = ConnectSQL.getResultSet(statement, "SELECT * FROM worldOfAutomation.employee where id =2;");
        if (!resultSet.next()){
            System.out.println("is Empty");
        }else{
            Assert.fail();
        }

    }
}
