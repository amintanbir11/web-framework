package com.worldofautomation.models;

public class  Employees {
    private int id;
    private String name;
    private boolean permanent;
    private int phone_number;
    private String role;
    private String city;

    public Employees(int id, String name, boolean permanent, int phone_number, String role, String city) {
        this.id = id;
        this.name = name;
        this.permanent = permanent;
        this.phone_number = phone_number;
        this.role = role;
        this.city = city;
    }

    @Override
    public String toString() {
        return "Employees{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", permanent=" + permanent +
                ", phone_number=" + phone_number +
                ", role='" + role + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
    public Employees(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isPermanent() {
        return permanent;
    }

    public void setPermanent(boolean permanent) {
        this.permanent = permanent;
    }

    public int getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(int phone_number) {
        this.phone_number = phone_number;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
