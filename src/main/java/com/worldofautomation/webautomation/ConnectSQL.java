package com.worldofautomation.webautomation;

import java.sql.*;
import java.util.Properties;

public class ConnectSQL {
    private static Properties properties;
    private static Connection connection = null;
    private static Statement statement = null;
    private static ResultSet resultSet = null;

    static {
        properties = Utilities.readPropertiesFile("src/main/resources/config.properties");
    }


    public static Connection getConnection(String databaseName) throws SQLException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ee) {
            System.out.println("please correct the driver name");
            ee.printStackTrace();
        }
        String url = "jdbc:mysql://localhost:3306/" + databaseName + "?serverTimezone=UTC";
        String userName = properties.getProperty("userName");
        String password = properties.getProperty("password");
        connection = DriverManager.getConnection(url, userName, password);
        return connection;
    }

    public static Statement getStatement(Connection connection) throws SQLException {
        statement = connection.createStatement();
        return statement;
    }

    public static ResultSet getResultSet(Statement statement, String query) throws SQLException {
        resultSet = statement.executeQuery(query);
        return resultSet;
    }

    public static void closeConnection() {

        try {
            if (resultSet != null) {
                resultSet.close();
            }
        } catch (SQLException ee) {
            ee.printStackTrace();
        }
        try {
            if (statement != null) {
                statement.close();
            }
        } catch (SQLException ee) {
            ee.printStackTrace();
        }
        try {
            if (connection != null) {
                connection.close();
            }

        } catch (SQLException ee) {
            ee.printStackTrace();
        }
        System.out.println("All the connections, statements and resultSet are closed");
    }

}
