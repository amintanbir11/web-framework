package com.worldofautomation.webautomation;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TestBase {

    private static WebDriver driver = null;
    private static ExtentReports extent;

    @Parameters({"url", "browserName", "cloud","os"})
    @BeforeMethod
    public static void setup(String url, String browserName, boolean cloud, String os) {
        if (cloud == true) {
            String bsUserName = "tanbiramin1";
            String bsAccessKey = "s27vqfLPuYJ2Gzxu8KMe";
            String urlForBS = "https://" + bsUserName + ":" + bsAccessKey + "@hub-cloud.browserstack.com/wd/hub";

            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setCapability("browserName", "chrome");
            caps.setCapability("browser_version", "80");
            caps.setCapability("os", "OS X");
            caps.setCapability("os_version", "High Sierra");
            caps.setCapability("resolution", "1024x768");
            caps.setCapability("name", "Cloud Execution Test");

            URL url1 = null;
            try {
                url1 = new URL(urlForBS);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            driver = new RemoteWebDriver(url1, caps);

        } else {
            if (os.equalsIgnoreCase("mac")) {
                if (browserName.equalsIgnoreCase("Chrome")) {
                    System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
                    driver = new ChromeDriver();
                } else if (browserName.equalsIgnoreCase("Mozilla")) {
                    System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver");
                    driver = new FirefoxDriver();
                }
            } else {
                if (browserName.equalsIgnoreCase("Chrome")) {
                    System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
                    driver = new ChromeDriver();
                } else if (browserName.equalsIgnoreCase("Mozilla")) {
                    System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver.exe");
                    driver = new FirefoxDriver();
                }
            }
            driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            driver.manage().window().maximize();
            driver.get(url);
        }
    }

    @AfterMethod
    public static void closeBrowser() {
        driver.quit();
    }

    public static WebDriver getDriver() {
        return driver;
    }

    //-----------------------Extent starts-------------------------------
    //Extent Report Setup
    @BeforeSuite(alwaysRun = true)
    public void extentSetup(ITestContext context) {
        ExtentTestManager.setOutputDirectory(context);
        extent = ExtentTestManager.getInstance();
    }

    //Extent Report Setup for each test cases / methods
    @BeforeMethod(alwaysRun = true)
    public void startExtent(Method method) {
        String className = method.getDeclaringClass().getSimpleName();
        ExtentTestManager.startTest(method.getName());
        ExtentTestManager.getTest().assignCategory(className);
    }

    //Extent Report cleanup for each test cases /methods
    @AfterMethod(alwaysRun = true)
    public void afterEachTestMethod(ITestResult result) {
        ExtentTestManager.getTest().getTest().setStartedTime(ExtentTestManager.getTime(result.getStartMillis()));
        ExtentTestManager.getTest().getTest().setEndedTime(ExtentTestManager.getTime(result.getEndMillis()));
        for (String group : result.getMethod().getGroups()) {
            ExtentTestManager.getTest().assignCategory(group);
        }

        if (result.getStatus() == 1) {
            ExtentTestManager.getTest().log(LogStatus.PASS, "TEST CASE PASSED : " + result.getName());
        } else if (result.getStatus() == 2) {
            ExtentTestManager.getTest().log(LogStatus.FAIL, "TEST CASE FAILED : " + result.getName() + " :: " + ExtentTestManager.getStackTrace(result.getThrowable()));
        } else if (result.getStatus() == 3) {
            ExtentTestManager.getTest().log(LogStatus.SKIP, "TEST CASE SKIPPED : " + result.getName());
        }
        ExtentTestManager.endTest();
        extent.flush();
        if (result.getStatus() == ITestResult.FAILURE) {
            ExtentTestManager.captureScreenshot(driver, result.getName());
        }
    }

    //Extent Report closed
    @AfterSuite(alwaysRun = true)
    public void generateReport() {
        ConnectSQL.closeConnection();
        extent.close();
    }

    //-----------------------Extent Ends-------------------------------


    public void pageScrollingByLinkText(String linkText) {
        WebElement text = driver.findElement(By.linkText(linkText));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView(true);", text);
    }

    public void typeUsingJs(String id, String value) {
        WebElement search = driver.findElement(By.id(id));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript(value, search);

    }

    public void clickUsingJsByLinkText(String linkText) {
        WebElement text = driver.findElement(By.linkText(linkText));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click();", text);
    }


    public static String getCurrentUrl() {
        String url = driver.getCurrentUrl();
        return url;
    }

    public boolean validateIfTheXpathIsDisplayed(String xpath) {
        boolean text = driver.findElement(By.xpath(xpath)).isDisplayed();
        return text;
    }

    public boolean validateIfTheListOfXpathIsDisplayed(String Xpath) {

        boolean flag = false;
        List<WebElement> elementList = driver.findElements(By.xpath(Xpath));
        for (int i = 0; i < elementList.size(); i++) {
            if (elementList.get(i).isDisplayed()) {
                flag = true;
            }
        }
        return flag;
    }

    public void validateIfTheTextIsDisplayed(String Xpath, String expectedText) {
        WebElement element = driver.findElement(By.xpath(Xpath));
        String actualText = element.getText();
        Assert.assertEquals(actualText, expectedText);
    }

    public void clickById(String id) {
        driver.findElement(By.id(id)).click();
    }

    public void clickByXpath(String xpath) {
        driver.findElement(By.xpath(xpath)).click();
    }

    public void clickByLinkText(String linkText) {
        driver.findElement(By.linkText(linkText)).click();
    }

    public void sendKeysById(String id, String keys) {
        driver.findElement(By.id(id)).sendKeys(keys);
    }

    public static void sendKeysByXpath(String xpath, String keys) {
        driver.findElement(By.xpath(xpath)).sendKeys(keys);
    }

    public String getText(String xpath) {
        String text = driver.findElement(By.xpath(xpath)).getText();
        return text;
    }

    public static void waitFor(int second) {
        try {
            Thread.sleep(second * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}

