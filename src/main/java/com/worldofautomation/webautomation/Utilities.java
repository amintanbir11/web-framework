package com.worldofautomation.webautomation;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.Properties;

public class  Utilities {

    public static void main(String[] args)  {
       JSONArray jsonArray = getJSONArray("src/test/resources/TestData.json" );
        System.out.println(jsonArray);
        JSONObject jsonObject = (JSONObject) jsonArray.get(2);
        System.out.println(jsonObject.get("SearchData"));

    }
    public static JSONArray getJSONArray(String filePath){
        JSONParser jsonParser = new JSONParser();
        FileReader fileReader = null;
        try {
            fileReader = new FileReader(filePath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        JSONArray jsonArray = null;
        try {
            jsonArray = (JSONArray) jsonParser.parse(fileReader);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return jsonArray;
    }
    public static Properties readPropertiesFile(String filePath) {
        Properties properties = new Properties();
        try {
            InputStream inputStream = new FileInputStream(filePath);
            properties.load(inputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }
    public static String readTextFile(String filePath) {
        String finalText = "";
        String tempContainer;

        try {
            FileReader fileReader = new FileReader(filePath);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            while ((tempContainer = bufferedReader.readLine()) != null) {
                finalText = finalText + "\n" + tempContainer;
            }

            bufferedReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return finalText;
    }


}
